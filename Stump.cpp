#include <vector>
#include <iostream>
#include <algorithm>
#include <random>
#include <boost/multi_array.hpp>
#include <boost/timer/timer.hpp>

const size_t n = 10000;
const size_t K = 2;

void initWeights(const boost::multi_array<int, 2>& Y,
	boost::multi_array<double, 2>& W)
{
	const size_t n = Y.size();
	const size_t K = Y.shape()[1];
	double oneDiv2n = 0.5 / n;
	double oneDiv2nK = 0.5 / (n * (K - 1));
	for (size_t i = 0; i < n; i++)
	{

		for (size_t l = 0; l < K; l++)
		{
			if (Y[i][l] > 0)
				W[i][l] = oneDiv2n;
			else
				W[i][l] = oneDiv2nK;
		}
	}
}

std::vector<double> runConstantHalfEdges(const boost::multi_array<int, 2>& Y,
	const boost::multi_array<double, 2>& W)
{
	const size_t n = Y.size();
	const size_t K = Y.shape()[1];
	std::vector<double> constantHalfEdges(K);
	for (size_t i = 0; i < n; i++)
	{
		for (size_t l = 0; l < K; l++)
		{
			constantHalfEdges[l] += W[i][l] * Y[i][l];
		}
	}
	for (size_t l = 0; l < K; l++)
	{
		constantHalfEdges[l] /= 2.0;
	}
	return constantHalfEdges;
}

double SingleStump(const std::vector<double>& X,
	const std::vector<size_t>& indices,
	const boost::multi_array<int, 2>& Y,
	const boost::multi_array<double, 2>& W,
	const std::vector<double>& constantHalfEdges)
{
	const size_t n = X.size();
	const size_t K = Y.shape()[1];
	double bestHalfEdge = std::numeric_limits<double>::quiet_NaN();
	size_t currPos, currNextPos;
	std::vector<double> halfEdges = constantHalfEdges;

	for (size_t ind = 0; ind < n - 1; ind++)
	{
		size_t i = indices[ind];
		size_t iplus = indices[ind + 1];
		double currHalfEdge = 0;
		for (size_t l = 0; l < K; l++)
		{
			halfEdges[l] -= W[i][l] * Y[i][l];
			currHalfEdge += fabs(halfEdges[l]);
		}
		if (X[i] != X[iplus])
		{
			if ((bestHalfEdge != bestHalfEdge) || (currHalfEdge > bestHalfEdge))
			{
				bestHalfEdge = currHalfEdge;
				currPos = i;
				currNextPos = iplus;
			}
		}
	}
	//if (bestHalfEdge == bestHalfEdge)
	//{
	//	std::cout << "Threshold " << (X[currPos] + X[currNextPos]) / 2 << std::endl;
	//	std::cout << currPos << std::endl;
	//}
	return bestHalfEdge;
}

std::random_device rd;
std::default_random_engine gen(rd());
std::uniform_real_distribution<> dis(0, 1);

void StumpBench(const size_t nmin, 
		const size_t nmax,
		const size_t Kmin,
		const size_t Kmax,
		const double timeout)
{
	for (size_t K = Kmin; K < Kmax; K *= 2)
	{
		for (size_t n = nmin; n < nmax; n *= 2)
		{
			if (n > nmin) std::cout << ",";
			std::vector<double> X(n);
			for (size_t i = 0; i < n; i++) X[i] = dis(gen);

			std::vector<size_t> indices(n);
			std::iota(indices.begin(), indices.end(), 0);

			std::sort(
				begin(indices), end(indices),
				[&](size_t a, size_t b) { return X[a] < X[b]; }
			);

			boost::multi_array<int, 2> Y(boost::extents[n][K]);
			for (size_t i = 0; i < n; i++)
				for (size_t j = 0; j < K; j++)
					if ((j * (n / K) <= i) && (i < ((j + 1) * (n / K))))
						Y[indices[i]][j] = 1;
					else
						Y[indices[i]][j] = -1;

			boost::multi_array<double, 2> W(boost::extents[n][K]);
			initWeights(Y, W);
			std::vector<double> constantHalfEdges = runConstantHalfEdges(Y, W);
			typedef boost::timer::nanosecond_type timer_res;
			auto besttime = std::numeric_limits<double>::max();
			boost::timer::cpu_timer t;
			size_t iterations = std::max(2000000 / (n * K), (size_t)2);
			double currentElapsed = 0;
			while (currentElapsed < timeout)
			{
				t.elapsed().clear();
				t.start();
				for (size_t j = 0; j < iterations; j++) SingleStump(X, indices, Y, W, constantHalfEdges);
				t.stop();
				auto tcur = boost::chrono::duration_cast<boost::chrono::duration<double>>(boost::chrono::nanoseconds(t.elapsed().user + t.elapsed().system)).count();
				if (tcur > 0 && tcur < besttime) besttime = tcur;
				currentElapsed += tcur;
			}
			std::cout << besttime / iterations;

		}
		std::cout << std::endl;
	}
}

int main()
{

	StumpBench(100, 10000, 2, 10000, 0.5);
	return 0;
}


// Local Variables:
// compile-command: "clang++ -O3 -std=c++11 Stump.cpp -o Stump -lboost_timer -lboost_system"
// eval: (c-set-style "stroustrup")
// End:


