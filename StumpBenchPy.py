import numpy as np
import timeit
import time
import sys

def initweights(Y):
    n = len(Y)
    K = np.shape(Y)[1]
    oneDiv2n = 0.5 / n
    oneDiv2nK = 0.5 / (n * (K - 1))
    return np.fromfunction(np.vectorize(lambda i,j: (oneDiv2n if Y[i,j] == 1 else oneDiv2nK)), (n,K), dtype=int)


def StumpBench(nmin, nmax, Kmin, Kmax,timeout,module):
    global X, indices, Y, W, constantHalfEdges
    t = timeit.Timer(stmt="stump(X,indices,Y,W,constantHalfEdges)",
                     setup="from %s import stump; from StumpBenchPy import X, indices, Y, W, constantHalfEdges"%(module), 
                     timer=time.process_time)
    f = open(module + "Py_results.txt","w")
    K = Kmin
    while (K < Kmax):
        # print(K)
        n = nmin
        while (n < nmax):
            # print("[",n,"]",end="")
            if (n > nmin):
                f.write(",")
            X = np.random.rand(n)
            indices = np.argsort(X)
            Y = np.zeros((n,K))
            for i in range(n):
                for j in range(K):
                    if ((j * (n / K)) <= i) and (i < ((j + 1)* (n / K))):
                        Y[indices[i],j] = 1
            Y = (Y * 2) - 1
            W = initweights(Y)
            constantHalfEdges = np.sum(W * Y, axis = 0)/2
            iterations = int(np.max([2000000 / (n * K), 2]))
            bestTime = np.inf
            currentElapsed = 0
            while (currentElapsed < timeout):
                currentDone = t.timeit(iterations)
                if ((currentDone > 0) and (currentDone < bestTime)):
                    bestTime = currentDone
                currentElapsed += currentDone
            f.write(str(bestTime / iterations))
            n *=2
        f.write("\n")
        K *=2    

StumpBench(100,10000,2,10000,0.5,"Stump_Python")
StumpBench(100,10000,2,10000,0.5,"Stump_Python_Numpy")
StumpBench(100,10000,2,10000,0.5,"Stump_Cython")
StumpBench(100,10000,2,10000,0.5,"Stump_Cython_Numpy")
StumpBench(100,10000,2,10000,0.5,"Stump_Cython_typed")
StumpBench(100,10000,2,10000,0.5,"Stump_Cython_Numpy_typed")
