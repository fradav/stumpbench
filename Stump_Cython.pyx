import numpy as np

def stump(X,indices,Y, W, constantHalfEdges):
    n = len(X)
    K = np.shape(Y)[1]
    halfEdges = constantHalfEdges
    bestHalfEdge = np.nan
    currPos = currNextPos = 0
    for ind in range(n-1):
        i = indices[ind]
        iplus = indices[ind + 1]
        currHalfEdge = 0
        for j in range(K):
            halfEdges[j] -= W[i,j] * Y[i,j]
            currHalfEdge += abs(halfEdges[j])
        if ((X[i] != X[iplus]) &
            (bestHalfEdge != bestHalfEdge) | (currHalfEdge > bestHalfEdge)):
                bestHalfEdge = currHalfEdge
                currPos = i
                currNextPos = iplus
    return bestHalfEdge

# Local Variables:
# mode: python
# compile-command: "python setupStump_Cython.py build_ext --inplace"
# End:
