import numpy as np
cimport numpy as np

def stump(np.ndarray[np.float64_t, ndim = 1] X,
          np.ndarray[Py_ssize_t, ndim = 1] indices,
          np.ndarray[np.float64_t, ndim = 2] Y, 
          np.ndarray[np.float64_t, ndim = 2] W,
          np.ndarray[np.float64_t, ndim = 1] constantHalfEdges):
    cdef Py_ssize_t n, K, ind, i, iplus, j, currPos, currNextPos
    cdef np.float64_t bestHalfEdge, currHalfEdge
    cdef np.ndarray[np.float64_t, ndim = 1] halfEdges
    n = len(X)
    K = np.shape(Y)[1]
    halfEdges = constantHalfEdges
    bestHalfEdge = np.nan
    currPos = currNextPos = 0
    for ind in range(n-1):
        i = indices[ind]
        iplus = indices[ind + 1]
        currHalfEdge = 0
        halfEdges -= W[i,:] * Y[i,:]
        currHalfEdge = np.sum(abs(halfEdges))
        if ((X[i] != X[iplus]) &
            (bestHalfEdge != bestHalfEdge) | (currHalfEdge > bestHalfEdge)):
            bestHalfEdge = currHalfEdge
            currPos = i
            currNextPos = iplus
    return bestHalfEdge

# Local Variables:
# mode: python
# compile-command: "python setupStump_Cython_Numpy_typed.py build_ext --inplace"
# End:
