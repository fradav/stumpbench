open System
open System.Diagnostics

let initWeights Y =
  let n = Array2D.length1 Y
  let K = Array2D.length2 Y
  let oneDiv2n = 0.5 / float(n)
  let oneDiv2nK = 0.5 / float(n * (K - 1))
  Y |>
  Array2D.map (fun y -> match y with
                         | 1 -> oneDiv2n
                         | _ -> oneDiv2nK)

let runConstantHalfEdges (Y:int[,]) (W:float[,]) =
  let n = Array2D.length1 Y
  let K = Array2D.length2 Y
  let constantHalfEdges = Array.zeroCreate K
  for i in 0 .. (n-1) do
    Array.iteri (fun j x -> constantHalfEdges.[j] <- x + W.[i,j] * float(Y.[i,j])) constantHalfEdges
  Array.map (fun x -> x / 2.) constantHalfEdges

let stump (X:float[]) (indices:int[]) (Y:int[,]) (W:float[,]) (constantHalfEdges:float[]) =
  let n = Array.length X
  let K = Array2D.length2 Y
  let halfEdges = constantHalfEdges
  let mutable bestHalfEdge = System.Double.NegativeInfinity
  let mutable currPos = 0
  for ind = 0 to (n-2) do
    let i = indices.[ind]
    let iplus = indices.[ind + 1]
    Array.iteri (fun j x -> halfEdges.[j] <- x - (W.[i,j] * float(Y.[i,j]))) halfEdges
    let currHalfEdge = Array.sumBy (System.Math.Abs:float->float) halfEdges
    if (X.[i] <> X.[iplus]) && (currHalfEdge > bestHalfEdge) then
      bestHalfEdge <- currHalfEdge
      currPos <- ind
  bestHalfEdge

let StumpBench nmin nmax kmin kmax timeout =
  let rand = new System.Random()
  let proc = System.Diagnostics.Process.GetCurrentProcess()
  let K = ref kmin
  while (!K < kmax) do
    let n = ref nmin
    while (!n < nmax) do
      if (!n > nmin) then printf ","
      let X = Array.init !n (fun _ -> rand.NextDouble())
      let indices = Array.sortBy (fun i -> X.[i]) [| 0 .. (!n-1) |]
      let Y = Array2D.init !n !K (fun i j -> match i,j with
                                             | i,j when ((float(j * !n) / float(!K)) <= float(i)) && (float(i) < (float((j + 1)* !n ) / float(!K)))  -> 1
                                             | _ -> -1)
      let W = initWeights Y
      let constantHalfEdges = runConstantHalfEdges Y W
      let iterations = int(System.Math.Max(2000000. / float(!n * !K), 2.0))
      let mutable bestTime = System.Double.PositiveInfinity
      let mutable currentElapsed = 0.
      while (currentElapsed < timeout) do
        let cpu_time_stamp = proc.TotalProcessorTime
        for i in 1 .. iterations do stump X indices Y W constantHalfEdges |> ignore
        let currentDone = float((proc.TotalProcessorTime - cpu_time_stamp).Ticks) / float(System.TimeSpan.TicksPerSecond) 
        if (currentDone > 0. && currentDone < bestTime) then bestTime <- currentDone
        currentElapsed <- currentElapsed + currentDone
      printf "%g" (bestTime / float(iterations))
      n := !n * 2
    printf "\n"
    K := !K * 2

StumpBench 100 10000 2 10000 0.5



// run with mono --llvm --gc=sgen Stump.exe 


// Local Variables:
// compile-command: "fsharpc --target:ext -O Stump.fs"
// End:
